'''
Kristina Amend - 0735252 
Trevor McCubbin - 1542960
'''

import pandas as pd
import matplotlib.pyplot as plt
from covid_database import MySqlConnection

"""
DataScience is a class that retrieves relevant information (as a DataFrame) from the tables in the covid database
It uses this information to display 3 different graphs containing COVID stats
"""


class DataScience:
    __corona_table = None
    __borders_table = None

    def __init__(self, hostname, username, password, database):
        self.__conn = MySqlConnection(host=hostname, user=username, password=password,
                                      database=database)
        self.__conn = MySqlConnection.get_connection(self.__conn)
        self.__get_data()

    # Retrieves the corona_table and country_borders_table table as a DataFrame from the database
    def __get_data(self):
        select_corona_table = "SELECT * FROM covid_corona_db_MCCU_AMEN.corona_table ORDER BY DATE ASC"
        select_borders_table = "SELECT * FROM covid_corona_db_MCCU_AMEN.country_borders_table"
        self.corona_table = pd.read_sql_query(select_corona_table, self.__conn)
        self.borders_table = pd.read_sql_query(select_borders_table, self.__conn)

    # Displays 3 important COVID totals for the given country
    # The country the user wants information on
    # Output: a graph displaying new cases, new recovered, new deaths for the country
    def case_evolution(self, country_name):
        try:
            current_country = self.corona_table['country'] == country_name
            select_country = self.corona_table[current_country]
            # retrieve DataFrame for the key indicators
            key_indicators = select_country[['new_cases', 'new_recovered', 'new_deaths', 'date']]
            key_indicators.plot(kind='bar', x='date', color=['#76bb86', '#febe3a', '#76858c'], figsize=(10, 6))
            plt.xlabel('Date', fontsize=12)
            plt.xticks(rotation=0)
            plt.ylabel('Total', fontsize=12)
            plt.legend(['New Cases', 'New Recovered', 'New Deaths'])
            plt.title(country_name + " 6 day evolution for 3 key indicators", fontsize=13)
            plt.show()
        except Exception as e:
            print("Insufficient data in the database for: " + country_name)

    # Displays the new cases from the country given, and the country with the longest border to country
    # The country the user wants information on
    # Output: a graph displaying new cases from both countries
    def longest_border_comparison(self, country_name):
        try:
            select_longest_border = "SELECT COUNTRY_BORDER_NAME FROM COUNTRY_BORDERS_TABLE WHERE COUNTRY_NAME = '" + country_name + "' ORDER BY BORDER_LENGTH DESC LIMIT 1"
            longest_bordering_country = pd.read_sql_query(select_longest_border, self.__conn)
            # make sure the country is found in the borders_table, otherwise raise error
            if longest_bordering_country.empty is not True:
                longest_bordering_country = longest_bordering_country.values[0][0]
                first_country = self.corona_table.loc[
                    self.corona_table['country'] == country_name, ['new_cases', 'date']]
                second_country = self.corona_table.loc[
                    self.corona_table['country'] == longest_bordering_country, ['new_cases', 'date']]
                # create a new DataFrame with both DataFrames combined
                plot_both_countries = pd.DataFrame({
                    "new_cases_first_country": first_country['new_cases'].values,
                    "new_cases_second_country": second_country['new_cases'].values,
                },  # index by date from either DataFrame (since the dates are the same)
                    index=first_country['date'].values
                )
                plot_both_countries.plot(kind="bar", color=['#febe3a', '#89a0e7'], figsize=(10, 6))
                plt.xlabel('Date', fontsize=12)
                plt.xticks(rotation=0)  # displays ticks as vertical for better readability
                plt.ylabel('New Cases', fontsize=12)
                plt.title(
                    "6 day comparison of new cases for " + country_name + '\n' + " and longest bordering neighbour: " + longest_bordering_country,
                    fontsize=13)
                plt.legend(['New Cases - ' + country_name, 'New Cases - ' + longest_bordering_country])
                plt.show()
            else:
                raise Exception
        except Exception as e:
            print("Insufficient data in the database for: " + country_name)

    # Displays the deaths per 1 million population from the country given, and 3 longest bordering countries
    # The country the user wants information on
    # Output: a graph displaying deaths per 1 million population for 4 countries
    def country_comp_3_neighbours(self, country_name):
        try:
            select_longest_borders = "SELECT COUNTRY_BORDER_NAME FROM COUNTRY_BORDERS_TABLE WHERE COUNTRY_NAME = '" + country_name + "' ORDER BY BORDER_LENGTH DESC LIMIT 3"
            longest_bordering_countries = pd.read_sql_query(select_longest_borders, self.__conn)
            if longest_bordering_countries.empty is not True:
                longest_bordering_countries = longest_bordering_countries.values
                # assign vars the country names, for easier string concat
                first_country = longest_bordering_countries[0][0]
                second_country = longest_bordering_countries[1][0]
                third_country = longest_bordering_countries[2][0]
                # retrieve all the info on "Deaths per 1 million population" for each respective country
                main_country_df = self.corona_table.loc[
                    self.corona_table['country'] == country_name, ['deaths_1M_pop', 'date']]
                first_country_df = self.corona_table.loc[
                    self.corona_table['country'] == first_country, ['deaths_1M_pop', 'date']]
                second_country_df = self.corona_table.loc[
                    self.corona_table['country'] == second_country, ['deaths_1M_pop', 'date']]
                third_country_df = self.corona_table.loc[
                    self.corona_table['country'] == third_country, ['deaths_1M_pop', 'date']]
                # create a new DataFrame with all 4 DataFrames combined
                plot_all_countries = pd.DataFrame({
                    "Deaths/1M pop0 - ": main_country_df['deaths_1M_pop'].values,
                    "Deaths/1M pop1 - ": first_country_df['deaths_1M_pop'].values,
                    "Deaths/1M pop2 - ": second_country_df['deaths_1M_pop'].values,
                    "Deaths/1M pop3 - ": third_country_df['deaths_1M_pop'].values,
                },  # index by date from either DataFrame (since the dates are the same)
                    index=main_country_df['date'].values
                )
                plot_all_countries.plot(kind="bar", figsize=(10, 6), color=['#9eb6ff', '#febe3a', '#a4dd57', '#ff7676'])
                plt.xlabel('Date', fontsize=12)
                plt.xticks(rotation=0)  # otherwise the x ticks are vertical
                plt.ylabel('Deaths/1M Population', fontsize=12)
                plt.title("6 days Deaths/1M pop comparison of " + country_name + '\n' + " and 3 bordering countries",
                          fontsize=13)
                plt.legend(['Deaths/1M Pop - ' + country_name, 'Deaths/1M Pop - ' + first_country,
                            'Deaths/1M Pop - ' + second_country, 'Deaths/1M Pop - ' + third_country])
                plt.show()
            else:
                raise Exception
        except Exception as e:
            print("Insufficient data in the database for: " + country_name)
