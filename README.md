## 420-420-DW Sec 01 Python - Server Side Programming
#### Project 1 - Pandemic Case Study (COVID-19)

*Created by: Trevor McCubbin ID 1542960 & Kristina Amend ID 0735252*

*Version: 1.0*

**An application written in Python that involves web scraping (BeautifulSoup), database management (mySQL), data analysis (Pandas, Matplotlib)**

### :computer: :date: :earth_americas: :mag: :bar_chart:

* An application that retrieves and analizes data from https://www.worldometers.info/coronavirus/
* Data for a specific country (user input) is displayed in 3 separate bar plots
* The user can continue retrieving data from local files, and choose a different country

## How to run:

0. Download this project (clone the repo) or from the Moodle/LEA submission

**https://gitlab.com/tmccubbin/python2021project1**

#### The user will need to enter their SQL credentials in one file:
1. Line: 21 [ main.py ]

#### To run the application
1. Open main.py
2. Select 'run' on main.py
3. Follow the sequence of options

**_Any exceptions will be displayed in the console_**
* _You may encounter errors when there is insufficient data available for the country in question_
* _(either it has no bordering countries, not enough data in the corona table, or it is not mutual between both database tables)_

#### To fully benefit from your experience, we recommend choosing from the following countries which:
1. Have no empty data values for the key indicators (new_cases, new_recovered, new_deaths, deaths_1m_pop)
2. Have at least 3 bordering countries (this is key for graph #3)

[ Albania, Algeria, Argentina, Armenia, Austria, Azerbaijan, Belarus, Belgium, Bolivia, Bosnia, 
Herzegovina, Brazil, Bulgaria, Cambodia, Chad, Chile, Colombia, Croatia, Egypt, Eritrea, Ethiopia,
Georgia, Germany, Ghana, Greece, Guatemala, Guyana, Honduras, Hungary, India, Indonesia, Iran, Iraq,
Israel, Italy, Jordan, Kazakhstan, Kenya, Latvia, Libya, Lithuania, Luxembourg, Malawi, Malaysia,
Mali, Mexico, Montenegro, Morocco, Mozambique, Namibia, Nigeria, North, Macedonia, Oman, Pakistan,
Palestine, Paraguay, Peru, Poland, Romania, Russia, Rwanda, Saudi, Arabia, Senegal, Serbia, Slovenia,
Somalia, South, Africa, Spain, Sudan, Switzerland, Syria, Thailand, Togo, Turkey, Ukraine, Zambia, Zimbabwe ]