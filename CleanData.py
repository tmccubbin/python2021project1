'''
Trevor McCubbin - 1542960
Kristina Amend - 0735252 
'''
import re

"""
Class that 'cleans' up the scraped data and formats it as a list
"""


class CleanData():

    # initializes the class variables
    def __init__(self, data, dates):
        self.__dirty_data = data
        self.__dates_to_append = dates
        self.__clean_data = self.cleanUpData()

    # This method is used to keep track of the steps in the cleaning of the data
    def cleanUpData(self):
        data = self.__removeChars(self.__dirty_data)
        data = self.__addNulls(data)
        data = self.__castFloats(data)
        data = self.__appendDates(data)
        data = self.__swap_rank_and_country(data)
        return data

    # This method removes all undesired chars in the data for each country
    def __removeChars(self, data):
        cleaned_list = []
        for country in data:
            for i, column in enumerate(country):
                # remove + and , within the column
                country[i] = column.replace('+', '').replace(',', '')
            cleaned_list.append(country)
        return cleaned_list

    # This method replaces all values that could be considered as null into None
    def __addNulls(self, data):
        cleaned_list = []
        for country in data:
            for i, column in enumerate(country):
                # if the column has no data make it null
                if len(column) == 0:
                    country[i] = None
                # if the column has no data (only a space) set it to null
                elif len(column) == 1 and column.find(' ') != -1:
                    country[i] = None
                # if the column is N/A set it to null
                elif column == 'N/A':
                    country[i] = None
            cleaned_list.append(country)
        return cleaned_list

    # Function that casts all strings representing numbers as floats
    # Floats are used because not all data is consistent to be casted as an int
    def __castFloats(self, data):
        for i in range(len(data)):
            for j in range(len(data[i])):
                # don't check None values
                if data[i][j]:
                    # check if column value is a string representation of a number
                    match = re.search("\d", data[i][j])
                    if match:
                        # cast to a float since there are numbers w/ decimals
                        data[i][j] = float(data[i][j])
        return data

    # Function that adds a date column to list of data
    def __appendDates(self, data):
        for i in range(len(data)):
            data[i].insert(0, self.__dates_to_append[i])
        return data

    # Function that swaps the rank column with country column (for SQL insert statements)
    def __swap_rank_and_country(self, data):
        rank_position = 1
        country_position = 2
        for row in range(len(data)):
            data[row][country_position], data[row][rank_position] = data[row][rank_position], data[row][
                country_position]
        return data

    # Returns the list of cleaned up data so it can be called from other classes
    def getCleanedData(self):
        return self.__clean_data
