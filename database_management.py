'''
Kristina Amend - 0735252 
Trevor McCubbin - 1542960
'''

from parse_json import JsonParse
from covid_database import MySqlConnection

"""
Class that manages DDL and DML statements for the database
"""


class DatabaseManagement():

    # initializes the Database Management class and takes both cleaned up soups as input
    def __init__(self, clean1, clean2, hostname, username, password, database):
        self.__parser = JsonParse()
        self.__parser.parse_country_json()
        # creates the db connection
        self.__conn = MySqlConnection(host=hostname, user=username, password=password, database=database)
        self.__clean1 = clean1
        self.__clean2 = clean2
        self.bordersTable()
        self.covidData()
        # closes the db connection
        self.__conn.close_connection()

    # Used to insert the borders table from the parsing of the JSON
    def bordersTable(self):
        table_name_borders = 'country_borders_table'

        table_schema_borders = '(country_name varchar(45) NOT NULL,' \
                               'country_border_name varchar(45),' \
                               'border_length decimal(10,0))'

        insert_schema_borders = '(country_name, country_border_name, border_length)'

        self.__conn.create_table(table_name_borders, table_schema_borders)
        self.__conn.populate_table(table_name_borders, insert_schema_borders, self.__parser.get_entries())

    # Used to insert the cleaned data into the table
    def covidData(self):
        table_name_countries = 'corona_table'

        table_schema_countries = "(date date NOT NULL," \
                                 "country varchar(45) NOT NULL," \
                                 "ranking int NOT NULL," \
                                 "total_cases decimal DEFAULT NULL," \
                                 "new_cases decimal DEFAULT NULL," \
                                 "total_deaths decimal DEFAULT NULL," \
                                 "new_deaths decimal DEFAULT NULL," \
                                 "total_recovered decimal DEFAULT NULL," \
                                 "new_recovered decimal DEFAULT NULL," \
                                 "active_cases decimal DEFAULT NULL," \
                                 "serious_critical decimal DEFAULT NULL," \
                                 "totalcases_1M_pop decimal DEFAULT NULL," \
                                 "deaths_1M_pop decimal DEFAULT NULL," \
                                 "total_tests decimal DEFAULT NULL," \
                                 "tests_1M_pop decimal DEFAULT NULL," \
                                 "population decimal DEFAULT NULL," \
                                 "continent varchar(45) DEFAULT NULL," \
                                 "case_every_x_ppl decimal DEFAULT NULL," \
                                 "death_every_x_ppl decimal DEFAULT NULL," \
                                 "test_every_x_ppl decimal DEFAULT NULL)"

        insert_schema_countries = '(date,country,ranking,total_cases,new_cases,total_deaths,new_deaths,total_recovered,' \
                                  'new_recovered, active_cases,serious_critical,totalcases_1M_pop,deaths_1M_pop,' \
                                  'total_tests,tests_1M_pop,population,continent,' \
                                  'case_every_x_ppl,death_every_x_ppl,test_every_x_ppl)'

        # creates the table
        self.__conn.create_table(table_name_countries, table_schema_countries)
        # inserts the cleaned data into the table
        self.__conn.populate_table(table_name_countries, insert_schema_countries, self.__clean1)
        # inserts the second cleaned data into the table
        self.__conn.populate_table(table_name_countries, insert_schema_countries, self.__clean2)
