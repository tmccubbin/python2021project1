'''
Kristina Amend - 0735252
Trevor McCubbin - 1542960
'''

"""
Class that writes a given list to a file (used for testing purposes only)
"""


class FileIO:

    # initializes the class variables
    def __init__(self, name):
        self.__filename = name

    # used for easy reading of the output from this program. Outputs all the countries to a file 'test.txt'
    def write2FileScrape(self, big_list):
        file = open(self.__filename, 'wt', encoding='utf-8')
        for l in big_list:
            file.writelines(str(l) + '\n')
        file.close()
