'''
Kristina Amend - 0735252
Trevor McCubbin - 1542960
'''

import File_IO
import Scraper
import CleanData
import database_management
from urllib.request import urlopen, Request
from bs4 import BeautifulSoup
from datetime import datetime, timedelta
from data_science import DataScience

# time in days to look for the other file. example: file 1 is for the 25th and it searches for file 2 on a day 3 days ago (22)
daydelta = 3

# database information
host = 'localhost'
user = 'root'
pword = 'password' # enter your password here
db = 'covid_corona_db_MCCU_AMEN'

def main():
    file1 = ''
    file2 = ''
    running = True
    while running == True:
        print('***** enter your choice:')
        print('1: scrape from live webpage')
        print('2: scrape from a saved local file')
        choice = 0
        while choice != 1 and choice != 2:
            try:
                choice = int(input('choice: '))
                continue
            except:
                print('Choice must be either 1 or 2')
                continue
            if choice > 2 or choice < 1:
                print('Choice must be either 1 or 2')

        # if the user chooses to pull directly from the webpage
        if choice == 1:
            print('***** enter your choice:')
            print('1: Use current webpage without saving')
            print('2: Save current webpage and continue')
            choice = 0
            while choice != 1 and choice != 2:
                try:
                    choice = int(input('choice: '))
                except:
                    print('Choice must be either 1 or 2')
                    continue
                if choice > 2 or choice < 1:
                    print('Choice must be either 1 or 2')
            url = 'https://www.worldometers.info/coronavirus/'
            headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'}
            print('Opening URL, Please Wait ...')
            request = Request(url, headers=headers)
            response = urlopen(request)
            webContent = response.read()
            soup = BeautifulSoup(webContent, "html.parser")
            # save the soup to a local file if the user chooses to
            if choice == 2:
                with open(".//local_html//local_page" + datetime.today().strftime('%Y-%m-%d') + ".html", "w", encoding='utf-8') as file_obj:
                    file_obj.write(str(soup))
                file1 = ".//local_html//local_page" + datetime.today().strftime('%Y-%m-%d') + ".html"
            else:
                file1 = soup
            old_date = datetime.today() - timedelta(days=daydelta)
            file2 = ".//local_html//local_page" + old_date.strftime('%Y-%m-%d') + ".html"

        # if the user decided to read a local html file
        elif choice == 2:
            print('what day would you like to review data for? 1-31')
            try:
                day = int(input('day: '))
            except:
                day = 0
            while day <= 0 or day > 31:
                print('Please input a valid day')
                day = int(input('day: '))
            # sets the first day to be the current year and current month and then the user input day
            day = datetime.today().replace(day=day)
            # sets the second day as 3 days ago
            day2 = day - timedelta(days=daydelta)
            file1 = ".//local_html//local_page" + day.strftime('%Y-%m-%d') + ".html"
            file2 = ".//local_html//local_page" + day2.strftime('%Y-%m-%d') + ".html"
        try:
            scrapeNSave(file1, file2)
        except:
            print('There is no file in the folder local_html for either ' + day.strftime('%Y-%m-%d') + ' or ' + day2.strftime('%Y-%m-%d') + ' . Please put one in the local_html folder and try again')
            continue
        try:
            analyze_data()
        except:
            print('Error in data science')
        print('Would you like to try a new date?')
        print('1: Yes')
        print('2. No')
        choice = 0
        while choice != 1 and choice != 2:
            try:
                choice = int(input('choice: '))
                continue
            except:
                print('Choice must be either 1 or 2')
                continue
            if choice > 2 or choice < 1:
                print('Choice must be either 1 or 2')
        if choice == 2:
            running = False

# scrapes the respective html files and then saves them to the db
def scrapeNSave(file1, file2):
    # first html file
    bs = Scraper.Scraper(file1)
    data = bs.getFetchedData()
    dates = bs.getDateColumns()
    cd = CleanData.CleanData(data, dates)
    cleaned = cd.getCleanedData()
        
    # second html file
    bs2 = Scraper.Scraper(file2)
    data2 = bs2.getFetchedData()
    dates2 = bs2.getDateColumns()
    cd2 = CleanData.CleanData(data2, dates2)
    cleaned2 = cd2.getCleanedData()

    # add cleaned data to database
    dm = database_management.DatabaseManagement(cleaned, cleaned2, hostname=host, username=user, password=pword, database=db)

# this method is for all data science related parts
def analyze_data():
    print('Section for analyzing data')
    analyzing = True
    while analyzing == True:
        country = str(input('Which Country would you like to analyze? '))
        ds = DataScience(hostname=host, username=user, password=pword, database=db)
        ds.case_evolution(country)
        ds.longest_border_comparison(country)
        ds.country_comp_3_neighbours(country)
        print('Would you like to try another country?')
        print('1. Yes')
        print('2. No')
        choice = int(input('choice: '))
        if choice == 2:
            analyzing = False
            print('Exiting section for analyzing data')

# this method should only be used if you want to put the cleaned up data into a file (used for debugging)
def file_io(cleaned, *cleaned2):
    io = File_IO.FileIO('scraped_data.txt')
    io.write2FileScrape(cleaned)
    #if cleaned2 is not provided it won't output the file
    if len(cleaned2) != 0:
        io = File_IO.FileIO('scraped_data2.txt')
        io.write2FileScrape(cleaned2)

main()