'''
Kristina Amend - 0735252 
Trevor McCubbin - 1542960
'''

import mysql
from mysql.connector import connect, Error

"""
Class to establish a mySQL connection
"""


class MySqlConnection:
    __host = None
    __user = None
    __password = None
    __database = None
    __connection = None
    __cursor = None

    # Initializes the default variables
    def __init__(self, **kwargs):
        self.__host = kwargs['host']
        self.__user = kwargs['user']
        self.__password = kwargs['password']
        if 'database' in kwargs:
            self.__database = kwargs['database']
        self.connection_db()

    # Function to establish a mySQL connection
    def connection_db(self):
        try:
            # if the database name is specified, login to that database
            if self.__database != None:
                try:
                    self.__connection = mysql.connector.connect(
                        host=self.__host,
                        user=self.__user,
                        password=self.__password,
                        database=self.__database
                    )
                    # set the cursor with the connection
                    self.__cursor = self.__connection.cursor()
                # if there is an error, connect to mysql without a database,
                # then create the database and select that database
                except Exception as err:
                    self.__connection = mysql.connector.connect(
                        host=self.__host,
                        user=self.__user,
                        password=self.__password
                    )
                    # set the cursor with the connection
                    self.__cursor = self.__connection.cursor()
                    self.create_db()
                    self.select_db(self.__database)
            # otherwise just connect to mysql
            else:
                self.__connection = mysql.connector.connect(
                    host=self.__hostname,
                    user=self.__username,
                    password=self.__password,
                )
                # set the cursor with the connection
                self.__cursor = self.__connection.cursor()
        except mysql.connector.Error as err:
            print('some problem with db: {}'.format(err))

    # create a db if it does not exist
    def create_db(self):
        # if the db name is not set, set it to the default value
        if self.__database is None:
            self.__database = 'covid_corona_db_MCCU_AMEN'
        create_db_query = "CREATE DATABASE " + self.__database
        # try creating the db
        try:
            self.__cursor.execute(create_db_query)
            self.__connection.commit()
        except Error as e:
            if e.errno == 1007:  # mySQL error number if database already exists
                # select the attempted database to create as the current
                self.select_db(self.__database)
            else:
                print('Problem with creating the database: ', e)

    # selects a specific database to be used
    def select_db(self, database_name):
        self.__database = database_name
        # try selecting the db
        try:
            self.__cursor.execute('USE ' + self.__database)
            self.__connection.commit()
        except mysql.connector.Error as err:
            print('some problem with db: {}'.format(err))

    # creates a table within the db you are currently connected to with the specified schema and table name
    def create_table(self, table_name, table_schema):
        # drops the table if it exists since the data wouldn't be accurate if we just added it to the current table
        create_statement = "DROP TABLE IF EXISTS " + table_name
        try:
            self.__cursor.execute(create_statement)
            self.__connection.commit()
        except Error as e:
            print("Error while dropping table ", e)
        # creates a table
        create_statement = "CREATE TABLE " + table_name + table_schema
        try:
            self.__cursor.execute(create_statement)
            self.__connection.commit()
        except Error as e:
            print("Error while creating table ", e)

    # Uses the provided table name, schema to insert the list_tuples into the db
    def populate_table(self, table_name, table_schema, list_tuples):
        try:
            # This creates a string of %s equivalent to the length of entries in a tuple
            # This is so the method doesn't need to be changed for multiple tables
            values = ','.join(['%s'] * (len(list_tuples[0])))
            # will insert all the values into a table, you can specify the table schema if you're not filling the
            # entire table. User need to put parenthesis
            insert_query = 'INSERT INTO ' + table_name + table_schema + ' VALUES (' + values + ')'
            self.__cursor.executemany(insert_query, list_tuples)
            self.__connection.commit()
        except mysql.connector.Error as err:
            print('Some problem inserting data into table: {}'.format(err))
            self.__connection.rollback()

    # closes the current MySQL connection
    def close_connection(self):
        try:
            self.__connection.close()
        except mysql.connector.Error as err:
            print('Some problem closing db connection: {}'.format(err))

    # returns the current connection to MySQL
    def get_connection(self):
        return self.__connection
