'''
Trevor McCubbin - 1542960
Kristina Amend - 0735252
'''

from urllib.request import urlopen
from bs4 import BeautifulSoup
from pathlib import Path
import re
import datetime
from datetime import timedelta

"""
Class that scrapes all data from the table found on https://www.worldometers.info/coronavirus/ (or a locally saved file)
"""


class Scraper:

    # initializes the class variables
    def __init__(self, file_to_scrape):
        if type(file_to_scrape) == str:
            self.__file_to_scrape = Path(file_to_scrape)
            self.__soup = self.__fetchSoup()
            self.__is_file = True
        else:
            self.__soup = file_to_scrape
            self.__is_file = False
        self.__fetched_data = self.__fetchData()
        self.__datesList = self.__addDates()

    # opens the website file stored locally into beautiful soup
    def __fetchSoup(self):
        with open(self.__file_to_scrape, 'rb') as fp:
            soup = BeautifulSoup(fp, 'html.parser')
        return soup

    # this method checks if the row is a continent or country (removes continents from list)
    def __checkContinent(self, item):
        # all this does is check if an item is empty or has data in it
        if len(item) == 0:
            return True
        else:
            return False

    # fetchs all the data that we need from te soup
    def __fetchData(self):
        l1 = []
        # finds all rows inside the html file
        rows = self.__soup.find_all('tr')
        # loop through every row found in the website
        for item in rows:
            l2 = []
            data = item.find_all('td')
            # for all the data entries in a row
            for i, item in enumerate(data):
                # if the row we are looking at is for a continent, ignore it
                if i == 0 and self.__checkContinent(item) == True:
                    break
                else:
                    l2.append(item.get_text())
            # only append countries to the list
            if len(l2) > 0:
                l1.append(l2)
        return l1

    # Function that creates a list of dates based on when the data was scraped (where the date is found in the filename)
    def __addDates(self):
        if self.__is_file:
            # find the date in the filename format yyyy-mm-dd
            date = re.search("\d{4}-\d{2}-\d{2}", str(self.__file_to_scrape))
            split_date = date.group().split('-')
        else:
            date = datetime.datetime.today().strftime('%Y-%m-%d')
            split_date = date.split('-')
        # create datetime for each day the data was scraped (first day, and 2 consecutive days)
        # datetime object is used to be able to increment days
        year_position = 0
        month_position = 1
        day_position = 2
        date_day1 = datetime.date(int(split_date[year_position]), int(split_date[month_position]),
                                  int(split_date[day_position]))
        date_day2 = date_day1 - timedelta(days=1)
        date_day3 = date_day1 - timedelta(days=2)
        # stringify the dates to properly use as sql inserts later as format YYYY-mm-dd
        date_day1 = date_day1.strftime('%Y-%m-%d')
        date_day2 = date_day2.strftime('%Y-%m-%d')
        date_day3 = date_day3.strftime('%Y-%m-%d')
        # find where each table 'ends' and the next series of fetched data begins (based on rank position)
        indices = []
        rank_index = 0
        # 221 represents where the last country in the table is found
        # each day is divided amongst countries ranked from 1-221 (inclusive)
        last_country_rank = '221'
        for i in range(len(self.__fetched_data)):
            if self.__fetched_data[i][rank_index] == last_country_rank:
                indices.append(i)
        # assigns index end position for the 3 table sections
        table1_end = indices[0]
        table2_end = indices[1]
        table3_end = indices[2]
        dates_list = []
        # append the corresponding dates in order from each table section
        for index in range(len(self.__fetched_data[0:table1_end + 1])):
            dates_list.append(date_day1)
        for index in range(len(self.__fetched_data[table1_end:table2_end + 1])):
            dates_list.append(date_day2)
        for index in range(len(self.__fetched_data[table1_end:table3_end])):
            dates_list.append(date_day3)
        return dates_list

    # returns fetched soup
    def getFetchedData(self):
        return self.__fetched_data

    # returns all the dates for the data
    def getDateColumns(self):
        return self.__datesList
