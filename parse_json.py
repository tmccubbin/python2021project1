'''
Kristina Amend - 0735252
Trevor McCubbin - 1542960
'''

import json

"""
Class that parses the given JSON file for countries/bordering countries
"""


class JsonParse:

    def __init__(self):
        self.__all_countries = []
        self.__all_borders = []
        self.__entries = []
        self.corona_database = None

    # Function that reads the file and converts it to a dictionary
    # returns a dictionary JSON file
    def __readFromFile(self):
        try:
            with open('./countries_json/country_neighbour_dist_file.json', 'r') as fileReader:
                json_records = json.load(fileReader)
                fileReader.close()
        except IOError:
            print('Unable to process file, please try again.')
        return json_records

    # function that parses a JSON file of country border data
    # returns a list of entries
    def parse_country_json(self):
        countries = self.__readFromFile()
        for country in countries:
            for borders in country.values():
                # creates a list of tuples of all the bordering countries
                if tuple(borders.items()):
                    self.__all_borders.append(tuple(borders.items()))
                else:
                    borders.update({None: None})
                    self.__all_borders.append(tuple(borders.items()))
                # creates a list of countries with duplicate entries
                # where the duplicate entry refers to how many bordering country each respective country has
                # ex: 'Afghanistan' has 8 bordering countries, it is added 8 times
                for i in range(len(borders.items())):
                    self.__all_countries.append(list(country.keys()))
        self.__prepare_sql_insert()

    # Function that prepares each list entry as a clean list of tuples (of strings) to later be used for SQL inserts
    def __prepare_sql_insert(self):
        current_country = 0
        row = 0
        while row < len(self.__all_borders):
            col = 0
            while col < len(self.__all_borders[row]):
                country = str(self.__all_countries[current_country]).strip('[]')
                borders = str(self.__all_borders[row][col]).strip('()')
                entry = country + ',' + borders
                self.__entries.append(entry)
                col += 1
                current_country += 1
            row += 1
            # convert the list of strings to a list of tuples
        self.__entries = list(map(eval, self.__entries))

    # returns the list of tuples
    def get_entries(self):
        return self.__entries
